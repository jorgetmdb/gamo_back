package gamo.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import gamo.web.configuracion.Configuracion;
import gamo.web.entidades.Usuario;
import gamo.web.servicios.UsuarioServiceInterface;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Configuracion.class)

public class TestUsuario {
	
	@Autowired
	private UsuarioServiceInterface usuarioServiceInterface;
	
	@Test
	public void crearUsuario(){
		Usuario e = new Usuario();
		e.setNombre("Noelia");
		e.setApellido1("Gonzalez");
		e.setApellido2(null);
		e.setContraseña("Noelia123");
		e.setCorreo("Noelia@hotmail.com");
		e.setDireccion("Calle inventanda 12");
		e.setDni("83494793N");
		e.setTelefono("3423423I392");
		getJpaUsuarioSI().save(e);
	}

	private UsuarioServiceInterface getJpaUsuarioSI() {
		// TODO Auto-generated method stub
		return usuarioServiceInterface;
	}
	
	@Test
	public void testTodosUsuarios()
	{
		List<Usuario> e = (List<Usuario>) getJpaUsuarioSI().findAll();
		for (Usuario usu : e)
		{
			System.out.println(usu.getNombre());
		}
	}
	
	@Test
	public void testUsuarioByID() {
		Optional<Usuario> e = Optional.ofNullable(new Usuario());
		e = getJpaUsuarioSI().findById(1);
		System.out.println(e.get().getNombre() + "@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
	
	@Test
	public void testModificarUsuario() {
		Usuario usuarioNuevo = new Usuario();
		usuarioNuevo.setNombre("Miguel Angel Modi");
		usuarioNuevo.setApellido1("Torres Modi");
		usuarioNuevo.setApellido2("Moraleja");
		usuarioNuevo.setContraseña("gamo123");
		usuarioNuevo.setCorreo("miguelAngelModi@hotmail.com");
		usuarioNuevo.setDireccion("Castilla la vieja 19, 7A Fuenlabrada");
		usuarioNuevo.setDni("034596895JModi");
		usuarioNuevo.setTelefono("663598754Modi");
		//usuarioServiceInterface.save(usuarioNuevo);
		
		int idUsuario = 2;
		Usuario usuario1 = usuarioServiceInterface.modificarUsuario(idUsuario,usuarioNuevo);
		
		assertEquals(idUsuario, usuario1.getId());
		//assertNotEquals(usuario1_id, usuario1.getId());

	}
	
	
}
