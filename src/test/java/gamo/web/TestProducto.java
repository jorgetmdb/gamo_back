package gamo.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import gamo.web.configuracion.Configuracion;
import gamo.web.entidades.Producto;
import gamo.web.servicios.ProductoServiceInterface;
import gamo.web.servicios.ProductoServiceInterface;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Configuracion.class)

public class TestProducto {

	@Autowired
	private ProductoServiceInterface productoServiceInterface;
	
	@Test
	public void crearProducto(){
		Producto e = new Producto();
		e.setNombre("Chorizo Iberico");
		e.setPrecio("9.99");
		getJpaProductoSI().save(e);
	}

	private ProductoServiceInterface getJpaProductoSI() {
		// TODO Auto-generated method stub
		return productoServiceInterface;
	}
	
	@Test
	public void testTodosProductos()
	{
		List<Producto> e = (List<Producto>) getJpaProductoSI().findAll();
		for (Producto usu : e)
		{
			System.out.println(usu.getNombre());
		}
	}
	
	@Test
	public void testProductoByID() {
		Optional<Producto> e = Optional.ofNullable(new Producto());
		e = getJpaProductoSI().findById(1);
		System.out.println(e.get().getNombre() + "@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
	
	
}
