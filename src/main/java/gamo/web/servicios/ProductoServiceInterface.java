package gamo.web.servicios;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gamo.web.entidades.Producto;
import gamo.web.repositorios.ProductoRepositoryInterface;

@Service
public interface ProductoServiceInterface {

    public Producto save(Producto producto);
	
	public Iterable<Producto> findAll();
	
	public Optional<Producto> findById(Integer id);
	
}
