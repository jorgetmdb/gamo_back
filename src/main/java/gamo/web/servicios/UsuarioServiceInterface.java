package gamo.web.servicios;

import java.util.Optional;

import org.springframework.stereotype.Service;

import gamo.web.entidades.Usuario;

@Service
public interface UsuarioServiceInterface {

	public Usuario save(Usuario usuario);
	
	public Iterable<Usuario> findAll();
	
	public Optional<Usuario> findById(Integer id);
	
	public Usuario modificarUsuario(int usuarioModificarId, Usuario nuevoUsuario);
}
