package gamo.web.servicios;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gamo.web.entidades.Usuario;
import gamo.web.repositorios.UsuarioRepositoryInterface;

@Service
public class UsuarioService implements UsuarioServiceInterface {

	@Autowired
	private UsuarioRepositoryInterface usuarioRepo;

	public UsuarioRepositoryInterface getUsuarioRepo() {
		return usuarioRepo;
	}

	public void setUsuarioRepo(UsuarioRepositoryInterface usuarioRepo) {
		this.usuarioRepo = usuarioRepo;
	} 
	
	public Usuario save(Usuario usuario)
	{
		return getUsuarioRepo().save(usuario);
	}

	public Iterable<Usuario> findAll()
	{
		// TODO Auto-generated method stub
		return getUsuarioRepo().findAll();
	}

	public Optional<Usuario> findById(Integer id) {
		return getUsuarioRepo().findById(id);
	}
	
	public Usuario modificarUsuario(int usuarioModificarId, Usuario nuevoUsuario) {
		nuevoUsuario.setId(usuarioModificarId);
		usuarioRepo.save(nuevoUsuario);
		return nuevoUsuario;
	}
}
