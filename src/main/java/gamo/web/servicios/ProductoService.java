package gamo.web.servicios;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gamo.web.entidades.Producto;
import gamo.web.repositorios.ProductoRepositoryInterface;
import gamo.web.repositorios.ProductoRepositoryInterface;

@Service
public class ProductoService implements ProductoServiceInterface {

	@Autowired
	private ProductoRepositoryInterface productoRepo;

	public ProductoRepositoryInterface getProductoRepo() {
		return productoRepo;
	}

	public void setProductoRepo(ProductoRepositoryInterface productoRepo) {
		this.productoRepo = productoRepo;
	}
	
	public Producto save(Producto producto)
	{
		return getProductoRepo().save(producto);
	}

	public Iterable<Producto> findAll()
	{
		// TODO Auto-generated method stub
		return getProductoRepo().findAll();
	}

	public Optional<Producto> findById(Integer id) {
		return getProductoRepo().findById(id);
	}
	
}
