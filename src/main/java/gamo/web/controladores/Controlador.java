package gamo.web.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gamo.web.entidades.Usuario;
import gamo.web.servicios.UsuarioService;


@RestController
@RequestMapping("/charcuteria")
@CrossOrigin(origins="http://localhost:4200")
public class Controlador {
	
	@Autowired
	private UsuarioService service;
	
	@CrossOrigin(origins="http://localhost:4200")
	@GetMapping("/usuarios")
	public Iterable<Usuario> listar(){
		return service.findAll();
	}
	
	@PostMapping("/crearUsuario")
	public ResponseEntity<?> save(@RequestBody Usuario usuario) {
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(usuario));
	}
}
