package gamo.web.repositorios;

import org.springframework.data.repository.CrudRepository;

import gamo.web.entidades.Producto;
import gamo.web.entidades.Usuario;

public interface ProductoRepositoryInterface extends CrudRepository<Producto, Integer>  {

}
